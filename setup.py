"""
`OSD Neo2` helps you learning the German ergonomic key mapping
`Neo Layout <http://neo-layout.org/>`_ by displaying the mapping
on-screen while you type. If you press a qualifier (e.g. shift, M3, M4)
it will display the mapping of the layer.
"""

from setuptools import setup, find_packages
from setuptools.command.sdist import sdist

from osd_neo2 import __version__ as version

class my_sdist(sdist):
    """Custom ``sdist`` command to ensure that mo files are always created."""

    def run(self):
        self.run_command('compile_catalog')
        # sdist is an old style class so super cannot be used.
        sdist.run(self)

setup(
    name = "OSD-neo2",
    version=version,
    install_requires = ['pygtk'],

    scripts = ['OSDneo2'],
    packages = ['osd_neo2'],
    include_package_data = True,

    cmdclass={'sdist': my_sdist},
    setup_requires=['Babel'],

    author = "Hartmut Goebel",
    author_email = "h.goebel@crazy-compilers.com",
    description = "On screen display for learning the keyboard layout Neo2 ",
    long_description = __doc__,
    license = "GPL 3.0",
    keywords = "keymap learning neo-layout",
    url          = "https://gitlab.com/htgoebel/OSD-Neo2/",
    download_url = "https://gitlab.com/htgoebel/OSD-Neo2/tags",
    classifiers = [
    'Development Status :: 5 - Production/Stable',
    'Environment :: X11 Applications :: GTK',
    'Intended Audience :: End Users/Desktop',
    'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
    'Natural Language :: English',
    'Natural Language :: German',
    'Operating System :: Linux',
    'Programming Language :: Python',
    'Topic :: Desktop Environment',
    'Topic :: Education',
    ],
)

# Local Variables:
# ispell-local-dictionary: "american"
# End:
