#


__all__ = ['package_path']
__version__ = '0.15.dev0'

import os

# initialise localisation settings
package_path = os.path.dirname(os.path.realpath(__file__))
